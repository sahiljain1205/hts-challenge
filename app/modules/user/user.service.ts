import { supabase } from "../../app";

export const createUser = async (
  firstName: string,
  lastName: string,
  email: string,
  password: string
) => {
  try {
    const { data: existingUser, error: checkError } = await supabase
      .from("users")
      .select("email")
      .eq("email", email)
      .maybeSingle();

    if (checkError) {
      throw checkError;
    }

    if (existingUser) {
      return { message: "User already exists", success: false };
    }

    const { data, error: insertError } = await supabase
      .from("users")
      .insert([
        { first_name: firstName, last_name: lastName, email, password },
      ]);

    if (insertError) {
      throw insertError;
    }

    return { message: "User created successfully", success: true };
  } catch (error) {
    throw { message: "Error creating user", details: error };
  }
};

export const logInUser = async (email: string, password: string) => {
  try {
    const { data: user, error } = await supabase
      .from("users")
      .select("*")
      .eq("email", email)
      .maybeSingle();

    if (error) {
      throw error;
    }

    if (!user) {
      return { message: "User does not exist", success: false };
    }

    if (user.password !== password) {
      return { message: "Incorrect password", success: false };
    }

    return { message: "Sign-in successful", success: true };
  } catch (error) {
    throw { message: "Error during sign-in", details: error };
  }
};
