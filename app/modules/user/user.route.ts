import { NextFunction, Request, Response, Router } from "express";
import { createUser, logInUser } from "./user.service";

const router = Router();

router.post(
  "/register",
  async (request: Request, response: Response, next: NextFunction) => {
    try {
      const { first_name, last_name, email, password } = request.body;

      if (!first_name || !last_name || !email || !password) {
        return response
          .status(400)
          .json({ message: "All fields are required" });
      }

      const result = await createUser(first_name, last_name, email, password);
      response.status(result.success ? 200 : 401).json(result);
    } catch (error) {
      response
        .status(500)
        .json({ message: "Failed to sign up user", error: error });
    }
  }
);

router.post(
  "/login",
  async (request: Request, response: Response, next: NextFunction) => {
    try {
      const { email, password } = request.body;
      if (!email || !password) {
        return response
          .status(400)
          .json({ message: "Email and password are required" });
      }

      const result = await logInUser(email, password);
      response.status(result.success ? 200 : 401).json(result);
    } catch (error) {
      response
        .status(500)
        .json({ message: "Failed to sign in user", error: error });
    }
  }
);

export default router;
