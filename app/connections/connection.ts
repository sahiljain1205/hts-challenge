import { createClient } from "@supabase/supabase-js";

export const connectToSupabase = async () => {
  try {
    const { SUPABASE_URL, SUPABASE_ANON_KEY } = process.env;

    if (!SUPABASE_URL || !SUPABASE_ANON_KEY) {
      throw new Error(
        "Supabase credentials are not set in environment variables"
      );
    }

    const supabase = createClient(SUPABASE_URL, SUPABASE_ANON_KEY);
    console.log("CONNECTED TO SUPABASE");

    return supabase;
  } catch (e) {
    throw { message: "FAILED TO CONNECT TO SUPABASE", error: e };
  }
};
