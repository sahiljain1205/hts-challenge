import { SupabaseClient } from "@supabase/supabase-js";
import bodyParser from "body-parser";
import express from "express";
import { connectToSupabase } from "./connections/connection";
import userRouter from "./modules/user/user.route";

export let supabase: SupabaseClient;

export const startServer = async () => {
  try {
    const app = express();

    supabase = await connectToSupabase();

    app.use(bodyParser.json());
    app.use("/user", userRouter);

    const { PORT } = process.env;
    app.listen(PORT, () => console.log(`SERVER STARTED ON PORT ${PORT ?? 3000}`));
  } catch (e) {
    console.error("UNABLE TO START SERVER");
    process.exit(1);
  }
};
